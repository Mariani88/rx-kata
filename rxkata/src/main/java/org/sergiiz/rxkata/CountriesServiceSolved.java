package org.sergiiz.rxkata;

import io.reactivex.Observable;
import io.reactivex.Single;

import java.util.List;
import java.util.Map;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

class CountriesServiceSolved implements CountriesService {

	private static final int ONE_MILLION = 1000000;
	private static final String DOLLAR = "USD";

	@Override
	public Single<String> countryNameInCapitals(Country country) {
		return Single.just(country.name.toUpperCase());
	}

	public Single<Integer> countCountries(List<Country> countries) {
		return Single.just(countries.size());
	}

	public Observable<Long> listPopulationOfEachCountry(List<Country> countries) {
		return Observable.fromIterable(countries).map(country -> country.population);
	}

	@Override
	public Observable<String> listNameOfEachCountry(List<Country> countries) {
		return Observable.fromIterable(countries).map(Country::getName);
	}

	@Override
	public Observable<Country> listOnly3rdAnd4thCountry(List<Country> countries) {
		int listThirdPosition = 2;
		int twoElements = 2;
		return Observable.fromIterable(countries).skip(listThirdPosition).take(twoElements);
	}

	@Override
	public Single<Boolean> isAllCountriesPopulationMoreThanOneMillion(List<Country> countries) {
		return Observable.fromIterable(countries).all(this::countryHasPopulationMoreThanOneMillions);
	}

	@Override
	public Observable<Country> listPopulationMoreThanOneMillion(List<Country> countries) {
		return Observable.fromIterable(countries).filter(this::countryHasPopulationMoreThanOneMillions);
	}

	@Override
	public Observable<Country> listPopulationMoreThanOneMillionWithTimeoutFallbackToEmpty(final FutureTask<List<Country>> countriesFromNetwork) {

		//OTRA IMPLEMENTACION
		/*return Observable.fromFuture(countriesFromNetwork).timeout(25, TimeUnit.MILLISECONDS, Observable.fromIterable(new ArrayList<>()))
				.flatMap(Observable::fromIterable).filter(this::countryHasPopulationMoreThanOneMillions).subscribeOn(Schedulers.io());*/

		return Observable.fromFuture(countriesFromNetwork, 25, TimeUnit.MILLISECONDS).onErrorResumeNext(Observable.empty())
				.flatMap(Observable::fromIterable).filter(this::countryHasPopulationMoreThanOneMillions);
	}

	@Override
	public Observable<String> getCurrencyUsdIfNotFound(String countryName, List<Country> countries) {
		return Observable.fromIterable(countries).filter(country -> countryName.equals(country.getName())).map(Country::getCurrency)
				.defaultIfEmpty(DOLLAR);
	}

	@Override
	public Observable<Long> sumPopulationOfCountries(List<Country> countries) {
		return Observable.fromIterable(countries).map(Country::getPopulation).reduce((population1, population2) -> population1 + population2)
				.toObservable();
	}

	@Override
	public Single<Map<String, Long>> mapCountriesToNamePopulation(List<Country> countries) {
		return Observable.fromIterable(countries).toMap(Country::getName, Country::getPopulation);
	}

	@Override
	public Observable<Long> sumPopulationOfCountries(Observable<Country> countryObservable1, Observable<Country> countryObservable2) {
		return countryObservable1.mergeWith(countryObservable2).map(Country::getPopulation)
				.reduce((population1, population2) -> population1 + population2).toObservable();
	}

	@Override
	public Single<Boolean> areEmittingSameSequences(Observable<Country> countryObservable1, Observable<Country> countryObservable2) {
		return Observable.sequenceEqual(countryObservable1, countryObservable2);
	}

	private boolean countryHasPopulationMoreThanOneMillions(Country country) {
		return country.population > ONE_MILLION;
	}
}
